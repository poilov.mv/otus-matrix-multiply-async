[![pipeline status](https://gitlab.com/poilov.mv/otus-matrix-multiply-async/badges/master/pipeline.svg)](https://gitlab.com/poilov.mv/otus-matrix-multiply-async/-/commits/master) 
[![coverage report](https://gitlab.com/poilov.mv/otus-matrix-multiply-async/badges/master/coverage.svg)](https://gitlab.com/poilov.mv/otus-matrix-multiply-async/-/commits/master)

# Multithreading

Домашнее задание по теме **Однопоточное и многопоточное приложения**

## Исходные файлы
### Функции расчета произведения матриц
* **source/MatrixMultiply.h** 
  * **sum** - сумма произведений элементов
  * **multiply** - синхронный расчет произведения 
  * **multiply_async** - асинхронный расчет произведения
### "Обёртка" над объектом потока
* **source/Thread.h**
* **source/Thread.cpp**
### Тесты
* **unit_tests/test_matrix_multiply.cpp**
* **unit_tests/test_thread.cpp**
