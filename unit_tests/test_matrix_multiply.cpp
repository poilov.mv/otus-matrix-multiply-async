//
// Created by Maxim on 14.11.2020.
//

#include "gtest/gtest.h"

#include "MatrixMultiply.h"

using namespace MatrixMultiply;

TEST(TestMatrixMultiply, can_multiply_with_indent_matrix) {
    const size_t N = 2;
    const size_t NN = N * N;

    int a[NN] = {1, 1,
                 1, 1};
    int b[NN] = {2, 2,
                 2, 2};

    int actualResult[NN];
    multiply<N>(a, b, actualResult);

    int expectedResult[NN] = {4, 4,
                              4, 4};
    for (int i = 0; i < NN; ++i) {
        ASSERT_EQ(expectedResult[i], actualResult[i]) << "row: " << (i / N) << " col: " << (i % N);
    }
}

TEST(TestMatrixMultiply, check_multiply_matrix_3_3_sync) {
    const size_t N = 3;
    const size_t NN = N * N;

    int a[NN] = { 2, 1, 4,
                 -5, 3, 8,
                  6, 2, 7};

    int b[NN] = { 9,-2, 5,
                  1, 0, 3,
                 -1, 6, 9};

    int actualResult[NN];
    multiply<N>(a, b, actualResult);

    int expectedResult[NN] = { 15, 20, 49,
                              -50, 58, 56,
                               49, 30, 99};
    for (int i = 0; i < NN; ++i) {
        ASSERT_EQ(expectedResult[i], actualResult[i]) << "row: " << (i / N) << " col: " << (i % N);
    }
}

TEST(TestMatrixMultiply, check_multiply_matrix_3_3_async) {
    const size_t N = 3;
    const size_t NN = N * N;

    int a[NN] = { 2, 1, 4,
                 -5, 3, 8,
                  6, 2, 7};

    int b[NN] = { 9,-2, 5,
                  1, 0, 3,
                 -1, 6, 9};

    int actualResult[NN];
    multiply_async<N>(a, b, actualResult);

    int expectedResult[NN] = { 15, 20, 49,
                              -50, 58, 56,
                               49, 30, 99};
    for (int i = 0; i < NN; ++i) {
        ASSERT_EQ(expectedResult[i], actualResult[i]) << "row: " << (i / N) << " col: " << (i % N);
    }
}

TEST(TestMatrixMultiply, check_multiply_matrix_N_100_async) {
    const size_t N = 100;
    const size_t NN = N * N;

    int a[NN];
    int b[NN];
    for (size_t i = 0; i < NN; ++i) {
        a[i] = (rand() % 1000) - 500;
        b[i] = (rand() % 1000) - 500;
    }

    int expectedResult[NN];
    multiply<N>(a, b, expectedResult);

    int actualResult[NN];
    multiply_async<N>(a, b, actualResult);

    for (int i = 0; i < NN; ++i) {
        ASSERT_EQ(expectedResult[i], actualResult[i]) << "row: " << (i / N) << " col: " << (i % N);
    }
}
