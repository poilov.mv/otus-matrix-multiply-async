//
// Created by Maxim on 14.11.2020.
//

#include "gtest/gtest.h"

#include "Thread.h"
#include <chrono>
#include <thread>

using namespace Threading;

TEST(TestThread, test_same_result_for_one_thread) {
    ThreadID main1_thread_id = Thread::currentThreadID();
    ThreadID main2_thread_id = Thread::currentThreadID();

    ASSERT_EQ(main1_thread_id, main2_thread_id);
}

TEST(TestThread, test_run_in_different_thread) {
    ThreadID main_thread_id = Thread::currentThreadID();
    ThreadID child_thread_id = Thread::currentThreadID();

    Thread thread([&child_thread_id]{
        Thread::sleep_ms(100);
        child_thread_id = Thread::currentThreadID();
    });
    
    thread.join();

    ASSERT_NE(main_thread_id, child_thread_id);
}

TEST(TestThread, test_run_in_several_threads) {
    ThreadID one_child_thread_id = Thread::currentThreadID();
    ThreadID another_child_thread_id = Thread::currentThreadID();

    Thread one_thread([&one_child_thread_id]{
        Thread::sleep_ms(100);
        one_child_thread_id = Thread::currentThreadID();
    });

    Thread another_thread([&another_child_thread_id]{
        Thread::sleep_ms(200);
        another_child_thread_id = Thread::currentThreadID();
    });
    
    one_thread.join();
    another_thread.join();

    ASSERT_NE(one_child_thread_id, another_child_thread_id);
}
