//
// Created by Maxim on 16.11.2020.
//

#ifndef MATRIX_MULTIPLY_ASYNC_MATRIXMULTIPLY_H
#define MATRIX_MULTIPLY_ASYNC_MATRIXMULTIPLY_H

#include "Thread.h"
#include <atomic>

namespace MatrixMultiply {

    template<size_t N>
    int sum(int a[N * N], int b[N * N], int row, int col) {
        int result = 0;
        for (int k = 0, a_index = row * N, b_index = col; k < N; ++k, a_index += 1, b_index += N) {
            result += a[a_index] * b[b_index];
        }
        return result;
    }

    template<size_t N>
    void multiply(int a[N * N], int b[N * N], int c[N * N]) {
        for (int i = 0; i < N * N; ++i) {
            c[i] = sum<N>(a, b, i / N, i % N);
        }
    }

    template<size_t N>
    void multiply_async(int a[N * N], int b[N * N], int c[N * N]) {
        std::atomic<int> counter(0);
        auto compute_next = [a, b, c, &counter]{
            while(true) {
                int i = counter.fetch_add(1, std::memory_order_relaxed);

                if (N * N <= i) { break; }

                c[i] = sum<N>(a, b, i / N, i % N);
            }
        };

        using Threading::Thread;
        Thread thread1(compute_next);
        Thread thread2(compute_next);
        Thread thread3(compute_next);
        Thread thread4(compute_next);
    }

};


#endif //MATRIX_MULTIPLY_ASYNC_MATRIXMULTIPLY_H
