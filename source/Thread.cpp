//
// Created by Maxim on 17.11.2020.
//

#include "Thread.h"

#if __GNUC__ || __MINGW32__ || __MINGW64__
    #include <time.h>
    #include <errno.h>
#else
    #include <chrono>
#endif

namespace Threading {

#if __GNUC__ || __MINGW32__ || __MINGW64__
    Thread::Thread(std::function<void()> f)
        : func(f) {
        status = pthread_create(&threadId_, NULL, Thread::thread_func, this);
    }
#else
    Thread::Thread(std::function<void()> f)
        : thread_(f) {
    }
#endif

    Thread::~Thread() {
        join();
    }

    void Thread::join() {
#if __GNUC__ || __MINGW32__ || __MINGW64__
        status = pthread_join(threadId_, NULL);
#else
        thread_.join();
#endif
    }

#if __GNUC__ || __MINGW32__ || __MINGW64__
    void *Thread::thread_func(void *d) {
        (static_cast <Thread *>(d))->func();
        return NULL;
    }
#endif

    ThreadID Thread::currentThreadID() {
#if __GNUC__ || __MINGW32__ || __MINGW64__
        return pthread_self();
#else
        return std::this_thread::get_id();
#endif
    }

    void Thread::sleep_ms(long msec) {
#if __GNUC__ || __MINGW32__ || __MINGW64__
        struct timespec ts;
        ts.tv_sec = msec / 1000;
        ts.tv_nsec = (msec % 1000) * 1000000;
        int res;
        do {
            res = nanosleep(&ts, &ts);
        } while(res && errno == EINTR);
#else
        std::this_thread::sleep_for(std::chrono::milliseconds(msec));
#endif
    }


}