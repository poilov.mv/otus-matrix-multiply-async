//
// Created by Maxim on 17.11.2020.
//

#ifndef MATRIX_MULTIPLY_ASYNC_THREAD_H
#define MATRIX_MULTIPLY_ASYNC_THREAD_H

#if __GNUC__ || __MINGW32__ || __MINGW64__
    #include <pthread.h>
#else
    #include <thread>
#endif
#include <functional>

namespace Threading {

    class ThreadID {
#if __GNUC__ || __MINGW32__ || __MINGW64__
        pthread_t id;
#else
        std::thread::id id;
#endif

    public:
        ThreadID() {}
#if __GNUC__ || __MINGW32__ || __MINGW64__
        ThreadID(const pthread_t &other)
                : id(other) {}
#else
        ThreadID(const std::thread::id &other)
                : id(other) {}
#endif
        ThreadID(const ThreadID &other)
                : id(other.id) {}

        bool operator==(const ThreadID &other) const {
#if __GNUC__ || __MINGW32__ || __MINGW64__
            return pthread_equal(id, other.id);
#else
            return id == other.id;
#endif
        }
        bool operator!=(const ThreadID &other) const {
            return !(*this == other);
        }
    };

    class Thread {
    public:
        Thread(std::function<void()> f);
        ~Thread();

        void join();

        static ThreadID currentThreadID();
        static void sleep_ms(long msec);

    protected:
        Thread() = delete;
        Thread(const Thread &) = delete;
        Thread &operator=(const Thread &) = delete;

    private:
#if __GNUC__ || __MINGW32__ || __MINGW64__
        std::function<void()> func;
        pthread_t threadId_;
        static void *thread_func(void *d);
        int status;
#else
        std::thread thread_;
#endif
    };

}

#endif //MATRIX_MULTIPLY_ASYNC_THREAD_H
